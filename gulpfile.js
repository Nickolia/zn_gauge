var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('js', function () {
    return gulp.src('gauge.js')
        .pipe(concat('gauge.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(''));
});
gulp.task('jquery', function () {
    return gulp.src('jquery.gauge.js')
        .pipe(concat('jquery.gauge.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(''));
});
gulp.task('minifying', ['js', 'jquery']);
